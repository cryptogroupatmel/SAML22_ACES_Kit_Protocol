// ----------------------------------------------------------------------------
//         ATMEL Crypto-Devices Software Support  -  Colorado Springs, CO -
// ----------------------------------------------------------------------------
// DISCLAIMER:  THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
// DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/** \file
 * \brief This file contains functions that parse ASCII protocol commands
 *        destined for an ECC108 device.
 * \author Atmel Crypto Products
 * \date October 24, 2013
 */

#include <asf.h>
#include <string.h>
#include <stdio.h>

#include "utilities.h"
#include "kitStatus.h"

// #include "cryptoauthlib.h"
// #include "test/atca_unit_tests.h"
// #include "test/atca_basic_tests.h"
// #include "test/atca_crypto_sw_tests.h"
// #include "host/atca_host.h"

//#include "config.h"

// #if TARGET_BOARD == NO_TARGET_BOARD
// #   error You have to define one of the target boards listed in config.h. 
// #endif
#include "parserAscii.h"
// #include "ecc108_comm.h"
// #include "ecc108_lib_return_codes.h"
 #include "ecc108_physical.h"
// #include "ecc108_comm_marshaling.h"
// #if TARGET_BOARD != AT88CK460
 #   include "Combined_Physical.h"
// #endif

#define ECC108_BUFFER_POS_COUNT      (0)                   //!< buffer index of count byte in command or response

extern ATCADevice _gDevice;
extern ATCACommand _gCommandObj;
extern ATCAIface _gIface;

/** \brief This flag causes this parser to wrap a Wakeup 
			and Idle around a command.
			
			We need this because of an ATECC108 SWI problem where the device
			goes to sleep before a USB host could send a "talk" message 
			after having received a USB reply to a Wakeup message (about 60 ms).
*/
static uint8_t send_wakeup_idle_with_command = 1;


/** \brief This function returns the size of the expected response in bytes,
 *         given a properly formatted ECC108 command.
 *
 * The data load is expected to be in Hex-Ascii and surrounded by parentheses.
 * \param[in] command pointer to the properly formatted ECC108 command buffer
 * \param[out] command_info pointer to structure for response size and execution time
 */
static void GetEcc108ResponseSize(uint8_t *command, command_info_t *command_info)
{
// 	// Get the Opcode and Param1
// 	uint8_t op_code = command[ECC108_OPCODE_IDX];
// 	uint8_t param1 = command[ECC108_PARAM1_IDX];
// 
// 	// Return the expected response size
// 	switch (op_code) {
// 	case ECC108_CHECKMAC:
// 		command_info->execution_time = CHECKMAC_EXEC_MAX;
// 		command_info->response_size = CHECKMAC_RSP_SIZE;
// 		break;
// 
// 	case ECC108_DERIVE_KEY:
// 		command_info->execution_time = DERIVE_KEY_EXEC_MAX;
// 		command_info->response_size = DERIVE_KEY_RSP_SIZE;
// 		break;
// 
// 	case ECC108_INFO:
// 		command_info->execution_time = INFO_EXEC_MAX;
// 		command_info->response_size = INFO_RSP_SIZE;
// 		break;
// 
// 	case ECC108_GENDIG:
// 		command_info->execution_time = GENDIG_EXEC_MAX;
// 		command_info->response_size = GENDIG_RSP_SIZE;
// 		break;
// 
// 	case ECC108_GENKEY:
// 		command_info->execution_time = GENKEY_EXEC_MAX;
// 		command_info->response_size = (param1 & GENKEY_MODE_DIGEST) 
// 				? ECC108_RSP_SIZE_MIN : GENKEY_RSP_SIZE_LONG;
// 		break;
// 
// 	case ECC108_HMAC:
// 		command_info->execution_time = HMAC_EXEC_MAX;
// 		command_info->response_size = HMAC_RSP_SIZE;
// 		break;
// 
// 	case ECC108_LOCK:
// 		command_info->execution_time = LOCK_EXEC_MAX;
// 		command_info->response_size = LOCK_RSP_SIZE;
// 		break;
// 
// 	case ECC108_MAC:
// 		command_info->execution_time = MAC_EXEC_MAX;
// 		command_info->response_size = MAC_RSP_SIZE;
// 		break;
// 
// 	case ECC108_NONCE:
// 		command_info->execution_time = NONCE_EXEC_MAX;
// 		command_info->response_size = ((param1 & NONCE_MODE_MASK) == NONCE_MODE_PASSTHROUGH
// 					? NONCE_RSP_SIZE_SHORT : NONCE_RSP_SIZE_LONG);
// 		break;
// 
// 	case ECC108_PAUSE:
// 		command_info->execution_time = PAUSE_EXEC_MAX;
// 		command_info->response_size = PAUSE_RSP_SIZE;
// 		break;
// 
// 	case ECC108_PRIVWRITE:
// 		command_info->execution_time = PRIVWRITE_EXEC_MAX;
// 		command_info->response_size = PRIVWRITE_RSP_SIZE;
// 		break;
// 
// 	case ECC108_RANDOM:
// 		command_info->execution_time = RANDOM_EXEC_MAX;
// 		command_info->response_size = RANDOM_RSP_SIZE;
// 		break;
// 
// 	case ECC108_READ:
// 		command_info->execution_time = READ_EXEC_MAX;
// 		command_info->response_size = (param1 & ECC108_ZONE_COUNT_FLAG 
// 			? READ_32_RSP_SIZE : READ_4_RSP_SIZE);
// 		break;
// 
// 	case ECC108_SIGN:
// 		command_info->execution_time = SIGN_EXEC_MAX;
// 		command_info->response_size = SIGN_RSP_SIZE;
// 		break;
// 
// 	//case ECC108_TEMPSENSE:
// 		//command_info->execution_time = TEMPSENSE_EXEC_MAX;
// 		//command_info->response_size = TEMPSENSE_RSP_SIZE;
// 		//break;
// 
// 	case ECC108_UPDATE_EXTRA:
// 		command_info->execution_time = UPDATE_EXEC_MAX;
// 		command_info->response_size = UPDATE_RSP_SIZE;
// 		break;
// 
// 	case ECC108_VERIFY:
// 		command_info->execution_time = VERIFY_EXEC_MAX;
// 		command_info->response_size = VERIFY_RSP_SIZE;
// 		break;
// 
// 	case ECC108_WRITE:
// 		command_info->execution_time = WRITE_EXEC_MAX;
// 		command_info->response_size = WRITE_RSP_SIZE;
// 		break;
// 		
// 	default:
// 		// Return the max size and execution time for commands 
// 		// that might be missing in the cases above.
// 		command_info->execution_time = 200;
// 		command_info->response_size = ECC108_RSP_SIZE_MAX;
// 		break;
// 	}
}


/** \brief This function parses communication commands (ASCII) received from a
 *         PC host and returns a binary response.
 *
 *         protocol syntax:\n\n
 *         functions for command sequences:\n
 *            v[erify]                            several Communication and Command Marshaling layer functions
 *            a[tomic]                            Wraps "talk" into a Wakeup / Idle.
 *         functions in sha204_comm.c (Communication layer):\n
 *            w[akeup]                            sha204c_wakeup\n
 *            t[alk](command)                     sha204c_send_and_receive\n
 *         functions in sha204_i2c.c / sha204_swi.c (Physical layer):\n
 *            [physical:]s[leep]                  sha204p_sleep\n
 *            [physical:]i[dle]                   sha204p_idle\n
 *            p[hysical]:r[esync]                 sha204p_resync\n
 *            p[hysical]:e[nable]                 sha204p_init\n
 *            p[hysical]:d[isable]                sha204p_disable_interface\n
 *            c[ommand](data)                     sha204p_send_command\n
 *            r[esponse](size)                    sha204p_receive_response\n
 * \param[in] commandLength number of bytes in command buffer
 * \param[in] command pointer to ASCII command buffer
 * \param[out] responseLength pointer to number of bytes in response buffer
 * \param[out] response pointer to binary response buffer
 * \return the status of the operation
 */

uint8_t ParseEccCommands(uint16_t commandLength, uint8_t *command, uint16_t *responseLength, uint8_t *response)
{
	ATCA_STATUS status;
//	uint8_t status = KIT_STATUS_SUCCESS;
	uint16_t dataLength;
	uint8_t *dataBuffer[1];
	uint8_t *dataLoad;
	command_info_t command_info;
	char *pToken = strchr((char *) command, ':');

	*responseLength = 0;

	if (!pToken)
		return KIT_STATUS_UNKNOWN_COMMAND;

	// Talk (send command and receive response)
	switch (pToken[1]) {
	case 't':
		status = ExtractDataLoad(pToken + 2, &dataLength, dataBuffer);
		if (status != KIT_STATUS_SUCCESS)
			return status;

		// Reset count byte.
		// For RhinoWhite, this overwrites the first byte of the command buffer since the USB rx and tx
		// buffers are shared. That's okay as long as the data load is not overwritten.
		response[0] = 0;

		// copy dataBuffer pointer
		uint8_t *pDataBuff = dataBuffer[0];

		// execute device command from software host
//		ecc108_execute_command(pDataBuff, response, responseLength);

		atcab_init( gCfg );
		ecc108_send_and_receive(pDataBuff, response);
		atcab_release();
		*responseLength = response[ECC108_BUFFER_POS_COUNT];

		break;

	// Wake up.
	case 'w':
		response[0] = 0x04; response[1] = 0x11; response[2] = 0x33; response[3] = 0x43;
		*responseLength = response[0];

		break;

	// The commands below are translated into calls to
	// Physical layer functions. But for downward compatibility
	// the "physical:" in the command string is optional.
	// send command
	case 'c':
// 		status = ExtractDataLoad(pToken + 2, &dataLength, dataBuffer);
// 		if (status != KIT_STATUS_SUCCESS)
// 			return status;
// 		dataLoad = dataBuffer[0];
// 		status = ecc108p_send_command((uint8_t) dataLength, dataLoad);
		break;

	// receive response
	case 'r':
// 		status = ExtractDataLoad(pToken + 2, &dataLength, dataBuffer);
// 		if (status != KIT_STATUS_SUCCESS)
// 			return status;
// 		// Reset count byte.
// 		response[ECC108_BUFFER_POS_COUNT] = 0;
// 		status = ecc108p_receive_response(*dataBuffer[0], response);
// 		*responseLength = response[ECC108_BUFFER_POS_COUNT];
		break;

	// Sleep
	case 's':
// 		if (send_wakeup_idle_with_command) {
// 			status = ecc108c_wakeup(response);
// 			if (status != KIT_STATUS_SUCCESS)
// 				break;
// 		}
// 		status = ecc108p_sleep();
		break;

	// Idle
	case 'i':
		atcab_init( gCfg );
		status = atcab_idle();
		atcab_release();

		break;
		
	// Switch whether to wrap a Wakeup / Idle around a "talk" message.
	case 'a':
// 		status = ExtractDataLoad(pToken + 2, &dataLength, dataBuffer);
// 		if (status != KIT_STATUS_SUCCESS)
// 			return status;
// 		send_wakeup_idle_with_command = *dataBuffer[0];
		break;


	// --------- functions in ecc108_i2c.c and ecc108_swi.c  --------------------
	case 'p':
		// ----------------------- "e[cc108]:p[hysical]:" ---------------------------
		pToken = strchr(&pToken[1], ':');
		if (!pToken)
			return KIT_STATUS_UNKNOWN_COMMAND;

		switch (pToken[1]) {
		// Wake-up without receive.
		case 'w':
// 			status = ecc108p_wakeup();
 			break;

		case 'c':
// 			// Send command.
// 			status = ExtractDataLoad(pToken + 2, &dataLength, dataBuffer);
// 			if (status != KIT_STATUS_SUCCESS)
// 				return status;
// 			dataLoad = dataBuffer[0];
// 			status = ecc108p_send_command((uint8_t) dataLength, dataLoad);
			break;

		// Receive response.
		case 'r':
// 			status = ExtractDataLoad(pToken + 2, &dataLength, dataBuffer);
// 			if (status != KIT_STATUS_SUCCESS)
// 				return status;
// 			// Reset count byte.
// 			response[ECC108_BUFFER_POS_COUNT] = 0;
// 			status = ecc108p_receive_response(*dataBuffer[0], response);
// 			*responseLength = response[ECC108_BUFFER_POS_COUNT];
			break;

		case 's':
 			if (pToken[2] == 'y') {
// 				// "sy[nc]"
// 				status = ecc108p_resync(ECC108_RSP_SIZE_MIN, response);
// 				*responseLength = (status == ECC108_SUCCESS ? response[ECC108_BUFFER_POS_COUNT] : 0);
 			}
 			else {
				// -- "s[elect](device index | TWI address)" or "s[leep]" ----------------
				status = ExtractDataLoad(pToken + 2, &dataLength, dataBuffer);
				if (status == KIT_STATUS_SUCCESS) {
					// Select device (I2C: address; SWI: index into GPIO array).
                    
                    // ECC108/ECC508 uses the SHA204 library
                    set_and_enable_interface(DEVKIT_LIB_SHA204, DEVKIT_IF_LAST);
                    
					// set device address
					// already done when performing atcab_init function in set_and_enable_interface function
					dataLoad = dataBuffer[0];
					ecc108p_set_device_id(dataLoad[0]);
 				}
				else {
					ATCADevice device;
					ATCAIface iface;
					ATCA_STATUS status;

					device = newATCADevice(gCfg);

					iface = atGetIFace(device);

					// Sleep command
					status = atsleep(iface);
				}
 			}
			break;


#if defined(ECC108) && defined(AES132) && !defined(PARSER_ONE_INTERFACE)
// indicates combined library support
// ---------------------- "{e[nable] | d[isable]} ---------------------
		case 'e':
// 			// Enables the current interface.
// 			ecc108p_init();
// 			status = KIT_STATUS_SUCCESS;
			break;

		case 'd':
// 			// Disables the current interface.
// 			ecc108p_disable_interface();
// 			status = KIT_STATUS_SUCCESS;
			break;

		case 'i':
// ---------------------- "i[nt]:{i[2c] | s[wi]} ------------------------
// 			pToken = strchr(&pToken[1], ':');
// 			if (pToken) {
// 				// Set and enable interface (I2C or SWI).
// 				// We are using the SHA204 interface since it is the same
// 				// as the ECC108 interface.
// 				if (pToken[1] == 'i')
// 					status = sha204p_set_interface(DEVKIT_IF_I2C);
// 				else if (pToken[1] == 's')
// 					status = sha204p_set_interface(DEVKIT_IF_SWI);
// 			}
// 			else {
// 				// Idle command
// 				status = ecc108p_idle();
// 			}
			break;
#endif

		default:
			status = KIT_STATUS_UNKNOWN_COMMAND;
			break;
			
		} // end switch physical
		
		break; // end case p

		
	default:
		status = KIT_STATUS_UNKNOWN_COMMAND;
		break;
	}
	
	return status;
}

void ecc108_execute_command(uint8_t *pDataBuffer, uint8_t *response, uint16_t *responseLength)
{
	ATCA_STATUS status;

	// execute specific ECC command
	switch (pDataBuffer[1])
	{
		case 0x30:
			atcab_init( gCfg );
			status = atcab_info( response );
			atcab_release();

			if (status == ATCA_SUCCESS)
			{
				*responseLength = response[ECC108_BUFFER_POS_COUNT];
			}

			break;

		case 0x02:
			atcab_init( gCfg );
			status = atcab_read_config_zone_aces(pDataBuffer, response);
			atcab_release();

			*responseLength = response[ECC108_BUFFER_POS_COUNT];
			break;

		default:
			break;
	}
}

/** \brief Send command and receive response.
 */

ATCA_STATUS ecc108_send_and_receive( uint8_t *pDataBuffer, uint8_t *response )
{
	ATCAPacket packet;
	ATCA_STATUS status = ATCA_GEN_FAIL;
	uint32_t execution_time;
	ATCA_CmdMap command_enum;

	if ( !_gDevice )
		return ATCA_GEN_FAIL;

	// build a command
	packet.param1 = pDataBuffer[2];
	packet.param2 = ( ((uint16_t) pDataBuffer[4]) <<8) + (uint16_t) pDataBuffer[3];

	do {
		if ( (status = atReport( pDataBuffer, _gCommandObj, &packet, &command_enum )) != ATCA_SUCCESS )
			break;

		execution_time = atGetExecTime( _gCommandObj, command_enum);

		if ( (status = atcab_wakeup()) != ATCA_SUCCESS )
			break;

		// send the command
		if ( (status = atsend( _gIface, (uint8_t*)&packet, packet.txsize )) != ATCA_SUCCESS )
			break;

		// delay the appropriate amount of time for command to execute
		atca_delay_ms(execution_time);

		// receive the response
		if ( (status = atreceive( _gIface, &(packet.data[0]), &(packet.rxsize) )) != ATCA_SUCCESS )
			break;

		// Check response size
		if (packet.rxsize < 4) {
			if (packet.rxsize > 0)
				status = ATCA_RX_FAIL;
			else
				status = ATCA_RX_NO_RESPONSE;
			break;
		}

		// OMIT THIS FUNCTION. to be able to get the response from the device
		// if the response is not ATCA_SUCCESS
// 		if ( (status = isATCAError(packet.data)) != ATCA_SUCCESS )
// 			break;

		memcpy( response, &packet.data[0], packet.rxsize );  // modify to copy all rx packet ([1]length + [4]payload + [2]crc)
	} while (0);

	if ( status != ATCA_COMM_FAIL )   // don't keep shoving more stuff at the chip if there's something wrong with comm
		atcab_idle();

	return status;
}

/** \brief HID Rx report method
 * \param[in] cacmd   instance
 * \param[in] packet  pointer to the packet containing the command being built
 * \return ATCA_STATUS
 */
ATCA_STATUS atReport( uint8_t *pDataBuffer, ATCACommand cacmd, ATCAPacket *packet, ATCA_CmdMap *command_enum )
{
	int macsize;
	int writesize;

	// Set the opcode & parameters
	switch (pDataBuffer[1] ) {
		
		// CHECKMAC
		case 0x28:
			*command_enum = CMD_CHECKMAC;
			// Set the opcode & parameters
			packet->opcode = ATCA_CHECKMAC;
			packet->txsize = pDataBuffer[0];
			packet->rxsize = CHECKMAC_RSP_SIZE;
			memcpy( &packet->data[0], pDataBuffer+5, CHECKMAC_CLIENT_CHALLENGE_SIZE );
			memcpy( &packet->data[32], pDataBuffer+37, CHECKMAC_CLIENT_RESPONSE_SIZE );
			memcpy( &packet->data[64], pDataBuffer+69, CHECKMAC_OTHER_DATA_SIZE );

			break;
		
		// COUNTER
		case 0x24:
			*command_enum = CMD_COUNTER;
			// Set the opcode & parameters
			packet->opcode = ATCA_COUNTER;
			packet->txsize = pDataBuffer[0];
			packet->rxsize = COUNTER_RSP_SIZE;

			break;

		// DERIVEKEY
		case 0x1C:
			*command_enum = CMD_DERIVEKEY;
			// Set the opcode & parameters
			packet->opcode = ATCA_DERIVE_KEY;
			packet->txsize = pDataBuffer[0];
			packet->rxsize = DERIVE_KEY_RSP_SIZE;

			break;

		// ECDH
		case 0x43:
			*command_enum = CMD_ECDH;
			// Set the opcode & parameters
			packet->opcode = ATCA_ECDH;
			packet->txsize = pDataBuffer[0];
			packet->rxsize = ECDH_RSP_SIZE;
			memcpy( &packet->data[0], pDataBuffer+5, ATCA_PUB_KEY_SIZE );

			break;

		// GENDIG
		case 0x15:
			*command_enum = CMD_GENDIG;
			// Set the opcode & parameters
			packet->opcode = ATCA_GENDIG;
			packet->txsize = pDataBuffer[0];
			packet->rxsize = GENDIG_RSP_SIZE;

			if ( packet->param1 == 0x03)
				memcpy( &packet->data[0], pDataBuffer+5, ATCA_WORD_SIZE );
			else if ( packet->param1 == 0x02) {
				memcpy( &packet->data[0], pDataBuffer+5, ATCA_WORD_SIZE );
			}

			break;

		// GENKEY
		case 0x40:
			*command_enum = CMD_GENKEY;
			// Set the opcode & parameters
			packet->opcode = ATCA_GENKEY;
			packet->txsize = pDataBuffer[0];
			packet->rxsize = GENKEY_RSP_SIZE_LONG;

			break;

		// HMAC
		case 0x11:
			*command_enum = CMD_HMAC;
			// Set the opcode & parameters
			packet->opcode = ATCA_HMAC;
			packet->txsize = pDataBuffer[0];
			packet->rxsize = HMAC_RSP_SIZE;

			break;

		// INFO
		case 0x30:
			*command_enum = CMD_INFO;
			// Set the opcode & parameters
			packet->opcode = ATCA_INFO;
			packet->txsize = pDataBuffer[0];
			packet->rxsize = INFO_RSP_SIZE;

			break;
		
		// LOCK
		case 0x17:
			*command_enum = CMD_LOCK;
			// Set the opcode & parameters
			packet->opcode = ATCA_LOCK;
			packet->txsize = pDataBuffer[0];
			packet->rxsize = LOCK_RSP_SIZE;

			break;

		// MAC
		case 0x08:
			*command_enum = CMD_MAC;
			// Set the opcode & parameters
			// variable packet size
			packet->opcode = ATCA_MAC;
			packet->txsize = pDataBuffer[0];
			packet->rxsize = MAC_RSP_SIZE;
			memcpy( &packet->data[0], pDataBuffer+5, 32 );  // a 32-byte challenge

			break;
		
		// NONCE
		case 0x16:
			*command_enum = CMD_NONCE;
			// Set the opcode & parameters
			// variable packet size
			packet->opcode = ATCA_NONCE;
			packet->txsize = pDataBuffer[0];

			int mode = packet->param1 & 0x03;
			if ( (mode == 0 || mode == 1) ) {       // mode[0:1] == 0 | 1 then NumIn is 20 bytes
				packet->rxsize = NONCE_RSP_SIZE_LONG;
			} else if ( mode == 0x03 ) {            // NumIn is 32 bytes
				packet->rxsize = NONCE_RSP_SIZE_SHORT;
			} else
				return ATCA_BAD_PARAM;

			memcpy( packet->data, pDataBuffer+5, 32 );

			break;
		
		// PAUSE
		case 0x01:
			*command_enum = CMD_PAUSE;
			// Set the opcode & parameters
			packet->opcode = ATCA_PAUSE;
			packet->txsize = pDataBuffer[0];
			packet->rxsize = PAUSE_RSP_SIZE;

			break;
		
		// PRIVWRITE
		case 0x46:
			*command_enum = CMD_PRIVWRITE;
			// Set the opcode & parameters
			packet->opcode = ATCA_PRIVWRITE;
			packet->txsize = pDataBuffer[0];
			packet->rxsize = PRIVWRITE_RSP_SIZE;
			memcpy( &packet->data[0], pDataBuffer+5, 36 );
			memcpy( &packet->data[36], pDataBuffer+41, 32 );
			
			break;

		// RANDOM
		case 0x1B:
			*command_enum = CMD_RANDOM;
			// Set the opcode & parameters
			packet->opcode = ATCA_RANDOM;
			packet->txsize = RANDOM_COUNT;
			packet->rxsize = RANDOM_RSP_SIZE;

			break;

		// READ
		case 0x02:
			*command_enum = CMD_READMEM;
			packet->opcode = ATCA_READ;
			packet->txsize = pDataBuffer[0];

			// variable response size based on read type
			if ((packet->param1 & 0x80) == 0 )
				packet->rxsize = READ_4_RSP_SIZE;
			else
				packet->rxsize = READ_32_RSP_SIZE;

			break;
		
		// SIGN
		case 0x41:
			*command_enum = CMD_SIGN;
			// Set the opcode & parameters
			packet->opcode = ATCA_SIGN;
			packet->txsize = pDataBuffer[0];
			// could be a 64 or 72 byte response depending upon the key configuration for the KeyID
			packet->rxsize = ATCA_RSP_SIZE_64;

			break;

		// SHA
		case 0x47:
			*command_enum = CMD_SHA;
			if ( packet->param2 > SHA_BLOCK_SIZE )
				return ATCA_BAD_PARAM;

			if ( packet->param1 == 0x01 && packet->param2 != SHA_BLOCK_SIZE )
				return ATCA_BAD_PARAM;                                              // updates should always have 64 bytes of data

			if ( packet->param1 == 0x02 && packet->param2 > SHA_BLOCK_SIZE - 1 )    // END should be 0-63 bytes
				return ATCA_BAD_PARAM;

			// Set the opcode & parameters
			packet->opcode = ATCA_SHA;

			switch ( packet->param1 ) {
				case 0x00: // START
					packet->rxsize = SHA_RSP_SIZE_SHORT;
					packet->txsize = pDataBuffer[0];
					break;
				case 0x01: // UPDATE
					packet->rxsize = SHA_RSP_SIZE_SHORT;
					packet->txsize = pDataBuffer[0];
					break;
				case 0x02: // END
					packet->rxsize = SHA_RSP_SIZE_LONG;
					// check the given packet for a size variable in param2.  If it is > 0, it should
					// be 0-63, incorporate that size into the packet
					packet->txsize = pDataBuffer[0];
					break;
			}

			break;
		
		// UPDATE EXTRA
		case 0x20:
			*command_enum = CMD_UPDATEEXTRA;
			// Set the opcode & parameters
			packet->opcode = ATCA_UPDATE_EXTRA;
			packet->txsize = pDataBuffer[0];
			packet->rxsize = UPDATE_RSP_SIZE;

			break;

		// VERIFY
		case 0x45:
			*command_enum = CMD_VERIFY;
			// Set the opcode & parameters
			packet->opcode = ATCA_VERIFY;
			packet->txsize = pDataBuffer[0];
			packet->rxsize = VERIFY_RSP_SIZE;
			memcpy( &packet->data[0], pDataBuffer+5, ATCA_SIG_SIZE);
			memcpy( &packet->data[64], pDataBuffer+69, ATCA_PUB_KEY_SIZE);

			break;

		// WRITE
		case 0x12:
			*command_enum = CMD_WRITEMEM;
			// Set the opcode & parameters
			packet->opcode = ATCA_WRITE;
			packet->txsize = pDataBuffer[0];
			packet->rxsize = WRITE_RSP_SIZE;
			memcpy( &packet->data[0], pDataBuffer+5, ATCA_KEY_SIZE);
			memcpy( &packet->data[32], pDataBuffer+37, ATCA_KEY_SIZE);

			break;
		
		default:
			break;
	}

	atCalcCrc( packet );
	return ATCA_SUCCESS;
}
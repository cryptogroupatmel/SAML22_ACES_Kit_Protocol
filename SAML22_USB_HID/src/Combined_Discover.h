// ----------------------------------------------------------------------------
//         ATMEL Crypto-Devices Software Support  -  Colorado Springs, CO -
// ----------------------------------------------------------------------------
// DISCLAIMER:  THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
// DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/** \file
 *  \brief 	This file contains definitions for device discovery.
 *  \author Atmel Crypto Products
 *  \date 	February 9, 2011
 */

#ifndef COMBINED_DISCOVER
#   define COMBINED_DISCOVER


// #include "Combined_Physical.h"
// #include "sha204_comm_marshaling.h"
#include "parserAscii.h"

//! lsb of TWI address is set when reading
#define I2C_READ_FLAG                    (1)

//! Wait the maximum execution time after sending a command during device discovery.
#define SHA204_EXEC_MAX        HMAC_EXEC_MAX

// Setting for Boston Engineering - Always SHA SWI
//#define SET_BENG

interface_id_t DiscoverDevices();
interface_id_t SetShaSwiDevice();
device_info_t *GetDeviceInfo(uint8_t index);
device_info_t* FindDeviceInfo(uint8_t address);
device_type_t GetDeviceType(uint8_t index);

#endif

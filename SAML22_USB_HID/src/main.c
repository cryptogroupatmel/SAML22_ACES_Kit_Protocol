/**
 * \file
 *
 * \brief Main functions for Generic example
 *
 * Copyright (c) 2011-2015 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */

#include <stdio.h>
#include <string.h>
#include <asf.h>
#include <ctype.h>

#include "conf_usb.h"
#include "ui.h"
#include "kitStatus.h"
#include "parserAscii.h"
#include "utilities.h"

#include "cryptoauthlib.h"
#include "test/atca_unit_tests.h"
#include "test/atca_basic_tests.h"
#include "test/atca_crypto_sw_tests.h"
#include "host/atca_host.h"

//! USB send data buffer
uint8_t pucUsbTxBuffer[USB_BUFFER_SIZE_TX];
//! USB receive data buffer
uint8_t pucUsbRxBuffer[USB_BUFFER_SIZE_RX+65];

static volatile bool main_b_generic_enable = false;

// The USART module instance
static struct usart_module usart_instance;

// USB HID message report pointer
uint8_t *pHID_report;
uint8_t HID_report_size;

/**
 * \brief Initializes the console EDBG USART interface.
 */
void console_init(void)
{
	struct usart_config usart_configuration;

	// Initialize the USART configuration
	usart_get_config_defaults(&usart_configuration);
	usart_configuration.baudrate    = 115200;
	usart_configuration.mux_setting = EDBG_CDC_SERCOM_MUX_SETTING;
	usart_configuration.pinmux_pad0 = EDBG_CDC_SERCOM_PINMUX_PAD0;
	usart_configuration.pinmux_pad1 = EDBG_CDC_SERCOM_PINMUX_PAD1;
	usart_configuration.pinmux_pad2 = EDBG_CDC_SERCOM_PINMUX_PAD2;
	usart_configuration.pinmux_pad3 = EDBG_CDC_SERCOM_PINMUX_PAD3;

	// Configure the USART module
	while (usart_init(&usart_instance, EDBG_CDC_MODULE,
	&usart_configuration) != STATUS_OK) {
	}

	// Initialize the stdio in serial mode
	stdio_serial_init(&usart_instance, EDBG_CDC_MODULE, &usart_configuration);

	// Enable the USART module
	usart_enable(&usart_instance);
}

/*! \brief Main function. Execution starts here.
 */
int main(void)
{
	irq_initialize_vectors();
	cpu_irq_enable();

	// Initialize the sleep manager
	sleepmgr_init();

#if !SAM0
	sysclk_init();
	board_init();
#else
	system_init();
#endif
	ui_init();
	ui_powerdown();

	console_init();
	printf("SAML22 firmware\n");
	
	// Start USB stack to authorize VBus monitoring
	udc_start();
	
	// The main loop manages only the power mode
	// because the USB management is done by interrupt
	while (true) {

		// check Rx packet numbers of the HID interrupt flag
		if ( get_hid_packetNum() )
		{
			// find ')' to verify end packet is already received 
			char *pToken = strchr((char *) pucUsbRxBuffer, ')');
			// if ')' is already received
			if (pToken)
			{
				// print USB HID Rx buffer report
				pucUsbRxBuffer[USB_BUFFER_SIZE_RX+64] = 0x00;
				printf("<< %s\n", pucUsbRxBuffer);
				
				// processing the Rx USB HID report
				process_packet();
				// restart Rx packet number
				set_hid_packetNum(0);
			}
			else
			{
				// if ')' isn't received, but packet buffer already reach 5 packets 
				if ( get_hid_packetNum() == 5 ) {
					printf ("Does not receive a valid command from ACES\n");
					// restart Rx packet numbers
					set_hid_packetNum(0);
				}
			}
		}

#ifdef   USB_DEVICE_LOW_SPEED
		// No USB "Keep a live" interrupt available in low speed
		// to scan generic interface then use main loop
		if (main_b_generic_enable) {
			static volatile uint16_t virtual_sof_sub = 0;
			static uint16_t virtual_sof = 0;
			if (sysclk_get_cpu_hz()/50000 ==
				virtual_sof_sub++) {
				virtual_sof_sub = 0;
				static uint16_t virtual_sof = 0;
				ui_process(virtual_sof++);
			}
		}
#else
		sleepmgr_enter_sleep();
#endif
	}
}

void main_suspend_action(void)
{
	ui_powerdown();
}

void main_resume_action(void)
{
	ui_wakeup();
}

void main_sof_action(void)
{
	if (!main_b_generic_enable)
		return;
	ui_process(udd_get_frame_number());
}

void main_remotewakeup_enable(void)
{
	ui_wakeup_enable();
}

void main_remotewakeup_disable(void)
{
	ui_wakeup_disable();
}

bool main_generic_enable(void)
{
	main_b_generic_enable = true;
	return true;
}

void main_generic_disable(void)
{
	main_b_generic_enable = false;
}

void main_hid_set_feature(uint8_t* report)
{
	if (report[0] == 0xAA && report[1] == 0x55
			&& report[2] == 0xAA && report[3] == 0x55) {
		// Disconnect USB Device
		udc_stop();
		ui_powerdown();
	}
}

void process_packet(void)
{
	uint8_t pBuffer[64];
	uint8_t reply_Times;
	uint8_t address_increment = 64;

	uint8_t status = KIT_STATUS_SUCCESS;
	uint8_t responseIsAscii = 0;
	uint16_t i;
	uint16_t txLength;
	uint16_t rxLength = 64;

	// set Tx buffer value into 0
	memset(pucUsbTxBuffer, 0, USB_BUFFER_SIZE_TX);

	// Start Process packet.
	for (i = 0; i < rxLength; i++)
		pucUsbRxBuffer[i] = tolower(pucUsbRxBuffer[i]);

	switch (pucUsbRxBuffer[0])
	{
		case 'e':	// "ecc108:"; parse raw ECC108 commands
			status = ParseEccCommands(rxLength, (uint8_t *) pucUsbRxBuffer, &txLength, pucUsbTxBuffer + 1);
			break;

		case 'b':
			// board level commands ("b[oard]")
			status = ParseBoardCommands((uint8_t) rxLength, (uint8_t *) pucUsbRxBuffer, &txLength, pucUsbTxBuffer, &responseIsAscii);
			break;

		default:
			status = KIT_STATUS_UNKNOWN_COMMAND;
			txLength = 1;
			break;
	}
	// End Process packet.

	// check if TxBuffer is already in Ascii format
	if (!responseIsAscii) {
		// Copy leading function return byte.
		pucUsbTxBuffer[0] = status;
		// Tell ConvertData the correct txLength.
		if (txLength < DEVICE_BUFFER_SIZE_MAX_RX)
			txLength++;
		txLength = ConvertData(txLength, pucUsbTxBuffer);
	}

	// print USB HID Tx buffer report
	pucUsbTxBuffer[txLength] = 0; //terminate the strings
 	printf("Response> %s\n", pucUsbTxBuffer);
	
	// send reply report to the host
	if ( txLength > 64 ) {
		// get remaining n Tx report
		reply_Times = txLength / 64;

		// send one 64 byte Tx report
		memcpy (pBuffer, pucUsbTxBuffer, 64);
		udi_hid_generic_send_report_in( pBuffer);
		// send n remaining Tx report
		while ( reply_Times > 0)
		{
			memcpy (pBuffer, pucUsbTxBuffer + address_increment, 64);
			do {
				// waiting for the usb hid report buffer free
			} while (!udi_hid_generic_send_report_in( pBuffer) );

			address_increment+=64;
			reply_Times--;
		}

// 		memcpy (pBuffer, pucUsbTxBuffer, 64);
// 		udi_hid_generic_send_report_in( pBuffer);
// 		memcpy (pBuffer, pucUsbTxBuffer + 64, 64);
// 		do {
// 		// waiting for the usb hid report buffer free
// 		} while (!udi_hid_generic_send_report_in( pBuffer) );
		
	} else {
		udi_hid_generic_send_report_in(pucUsbTxBuffer);
	}

	// reset Rx buffer
	memset(pucUsbRxBuffer, 0, USB_BUFFER_SIZE_RX+1);

}

/** \brief This function converts binary data to Hex-ASCII.
 * \param[in] length number of bytes to send
 * \param[in] buffer pointer to tx buffer
 * \return new length of data
 */
static uint16_t ConvertData(uint16_t length, uint8_t *buffer)
{
	if (length > DEVICE_BUFFER_SIZE_MAX_RX) {
		buffer[0] = KIT_STATUS_USB_TX_OVERFLOW;
		length = DEVICE_BUFFER_SIZE_MAX_RX;
	}
	return CreateUsbPacket(length, buffer);
}

/** \brief This function converts binary response data to hex-ascii and packs it into a protocol response.
           <status byte> <'('> <hex-ascii data> <')'> <'\n'>
    \param[in] length number of bytes in data load plus one status byte
    \param[in] buffer pointer to data
    \return length of ASCII data
*/
uint16_t CreateUsbPacket(uint16_t length, uint8_t *buffer)
{
	uint16_t binBufferIndex = length - 1;
	// Size of data load is length minus status byte.
	uint16_t asciiLength = 2 * (length - 1) + 5; // + 5: 2 status byte characters + '(' + ")\n"
	uint16_t asciiBufferIndex = asciiLength - 1;
	uint8_t byteValue;

	// Terminate ASCII packet.
	buffer[asciiBufferIndex--] = KIT_EOP;

	// Append ')'.
	buffer[asciiBufferIndex--] = ')';

	// Convert binary data to hex-ascii starting with the last byte of data.
	while (binBufferIndex)
	{
		byteValue = buffer[binBufferIndex--];
		buffer[asciiBufferIndex--] = ConvertNibbleToAscii(byteValue);
		buffer[asciiBufferIndex--] = ConvertNibbleToAscii(byteValue >> 4);
	}

	// Start data load with open parenthesis.
	buffer[asciiBufferIndex--] = '(';

	// Convert first byte (function return value) to hex-ascii.
	byteValue = buffer[0];
	buffer[asciiBufferIndex--] = ConvertNibbleToAscii(byteValue);
	buffer[asciiBufferIndex] = ConvertNibbleToAscii(byteValue >> 4);

	return asciiLength;
}

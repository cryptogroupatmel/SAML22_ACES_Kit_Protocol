// ----------------------------------------------------------------------------
//         ATMEL Crypto_Devices Software Support  -  Colorado Springs, CO -
// ----------------------------------------------------------------------------
// DISCLAIMER:  THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
// DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
// EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/** \file
 * Microbase module for discovering devices.
 * \author Atmel Crypto Products
 * \date October 17, 2013
*/

//#include <avr/io.h>
#include <asf.h>
#include <string.h>
#include <stdio.h>

// #include "config.h"
#include "Combined_Discover.h"
// #include "delay_x.h"
// #include "i2c_phys.h"

// #include "aes132_lib_return_codes.h"
// #include "aes132_commands.h"
// #include "aes132_physical.h"

// #include "sha204_comm_marshaling.h"
// #include "sha204_comm.h"
// #include "swi_phys.h"
// #include "sha204_lib_return_codes.h"

// #if TARGET_BOARD==0
// #   error You have to define TARGET_BOARD > 0
// #endif


// declared in sha204_comm.c
extern void sha204c_calculate_crc(uint8_t length, uint8_t *data, uint8_t *crc);

// declared in sha204_bitbang_physical.c
extern uint8_t swi_get_pin_array_size();


//uint8_t receivebuf[SHA204_RSP_SIZE_MAX];

//uint8_t sha204_devrev_command[SHA204_CMD_SIZE_MIN] = {
//			SHA204_CMD_SIZE_MIN, SHA204_DEVREV, 0, 0, 0, 0x03, 0x5D};
// uint8_t sa10_genkey_command[23] = {
// 			23, 32, 0, 0, 0,
// 			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
// 			0x00, 0x00};
//device_info_t device_info[DISCOVER_DEVICE_COUNT_MAX];
device_info_t device_info[2];
uint8_t device_count = 0;

// start_initiation i2c device discovery
uint8_t wake_com_expected[4] = { 0x04, 0x11, 0x33, 0x43 };
uint8_t info_command[] = {0x03, 0x07, 0x30, 0x00, 0x00, 0x00, 0x03, 0x5D};
uint8_t info_command_aes132[] = {0xFE, 0x00, 0x09, 0x0C, 0x00, 0x00, 0x06, 0x00, 0x00, 0xA9, 0xE7};

// i2c master driver instance
struct i2c_master_module i2c_master_discover;
// i2c master device configuration
struct i2c_master_config config_i2c_master_discover;
// end_initiation i2c device discovery

//! This macro updates the device type.
//#define UPDATE_DEVICE_TYPE device_info[device_count].device_type = device_info[device_count].dev_rev[2] == 0 ? DEVICE_TYPE_SHA204 : DEVICE_TYPE_ECC108

static device_type_t DevRevToDeviceType(const uint8_t dev_rev[4])
{
	uint32_t dev_rev_num = ((uint32_t)dev_rev[0] << 24) | ((uint32_t)dev_rev[1] << 16) | ((uint32_t)dev_rev[2] << 8) | ((uint32_t)dev_rev[3] << 0);

	if (dev_rev_num >= 0x00000000 && dev_rev_num < 0x00020008)
		return DEVICE_TYPE_SHA204; // ATSHA204
	if (dev_rev_num >= 0x00020008 && dev_rev_num < 0x00030000)
		return DEVICE_TYPE_SHA204; // ATSHA204A
	if (dev_rev_num >= 0x00004000 && dev_rev_num < 0x00005000)
		return DEVICE_TYPE_SHA204; // ATCPZ002

	if (dev_rev_num >= 0x00001000 && dev_rev_num < 0x00001002)
		return DEVICE_TYPE_ECC108; // ATECC108
	if (dev_rev_num >= 0x00001002 && dev_rev_num < 0x00001100)
		return DEVICE_TYPE_ECC108; // ATECC108A
	if (dev_rev_num >= 0x00005000 && dev_rev_num < 0x00005100)
		return DEVICE_TYPE_ECC108; // ATECC508A

	// Default to ECC
	return DEVICE_TYPE_ECC108;
}

static bool IsNoPoll(const uint8_t dev_rev[4])
{
// 	uint32_t dev_rev_num = ((uint32_t)dev_rev[0] << 24) | ((uint32_t)dev_rev[1] << 16) | ((uint32_t)dev_rev[2] << 8) | ((uint32_t)dev_rev[3] << 0);
// 
// 	// Can't poll on ATCPZ002 devices
// 	return (dev_rev_num >= 0x00004000 && dev_rev_num < 0x00005000);
}

device_info_t *GetDeviceInfo(uint8_t index) {
	if (index >= device_count)
		return NULL;
	return &device_info[index];
}

device_info_t* FindDeviceInfo(uint8_t address)
{
// 	for (int i = 0; i < device_count; i++)
// 		if (device_info[i].address == address)
// 			return &device_info[i];
// 	return NULL;
}

device_type_t GetDeviceType(uint8_t index) {
// 	if (index >= device_count)
// 		return DEVICE_TYPE_UNKNOWN;
// 	return device_info[index].device_type;
}


/** This function checks the consistency of a SHA204 / ECC108 response.
 * \param[in] response pointer to response buffer
 * \return status of the operation
 */
uint8_t VerifyResponse(uint8_t *response)
{
// 	uint8_t crc[SHA204_CRC_SIZE];
// 
// 	// Check count byte.
// 	uint8_t count = response[SHA204_COUNT_IDX];
// 	if (count < SHA204_RSP_SIZE_MIN || count > SHA204_RSP_SIZE_MAX)
// 		return SHA204_INVALID_SIZE;
// 
// 	// Check CRC.
// 	count -= SHA204_CRC_SIZE;
// 	sha204c_calculate_crc(count, response, crc);
// 	return (*((uint16_t *) crc) == *((uint16_t *) (response + count)))
// 		? SHA204_SUCCESS : SHA204_BAD_CRC;
}


/** \brief This function tries to detect a SHA204 or ECC108 SWI device.
 *
 *         This function assumes the bit-banging version of the SWI
 *         (sha204_bitbang_physical.c).
 *         Therefore we do not need to call an initialization function.
 *  \return number of devices found
 */
uint8_t DetectSwiDevices(void)
{
// 	if (device_count >= DISCOVER_DEVICE_COUNT_MAX)
// 		return 0;
// 
// 	static uint8_t lib_return;
// 	uint8_t command[SHA204_CMD_SIZE_MIN];
// 	uint8_t response[SHA204_RSP_SIZE_VAL];
// 	uint8_t device_id;
// 	uint8_t pin_count = swi_get_pin_array_size();
// 
// 	// ------------------ Detect SWI devices. -----------------
// 
// 	// Set the interface.
// 	sha204p_set_interface(DEVKIT_IF_SWI);
// 
// 	// Wakeup the SHA204 device and obtain its status by
// 	// sending a TX flag. Do this for every pin in the
// 	// pin array declared in sha204_bitbang_physical.c.
// 	// This implementation does not support two devices on the same pin.
// 	for (device_id = 0; device_id < pin_count; device_id++) {
// 
// 		// Enable interface.
// 		sha204p_init();
// 
// 		sha204p_set_device_id(device_id);
// 
// 		//  Wake up device.
// 		lib_return = sha204c_wakeup(response);
// 		if (lib_return != SHA204_SUCCESS) {
// 			sha204p_sleep();
// 			sha204p_disable_interface();
// 			continue;
// 		}
// 
// 		// Delay turn-around time.
// 		_delay_us(100);
// 
// 		// We found a device.
// 		// Send a SHA204 "Device Revision" command (same as ECC108 "Info") and receive its response.
// 		// We don't use sha204c_send_and_receive() because this function would retry
// 		// even if we woke up an SA10xS device.
// 		lib_return = sha204p_send_command(sha204_devrev_command[SHA204_COUNT_IDX], sha204_devrev_command);
// 		if (lib_return == SHA204_SUCCESS) {
// 			_delay_ms(DEVREV_EXEC_MAX);
// 			lib_return = sha204p_receive_response(sizeof(response), response);
// 			if (lib_return == SHA204_SUCCESS) {
// 				// We found a SHA204 or ECC108 device.
// 				if (VerifyResponse(response) == SHA204_SUCCESS) {
// 					// We got a valid response to a DevRev command. We found a SHA204 device.
// 					memcpy(device_info[device_count].dev_rev, &response[SHA204_BUFFER_POS_DATA], sizeof(device_info[device_count].dev_rev));
// 					device_info[device_count].bus_type = DEVKIT_IF_SWI;
// 					device_info[device_count].device_type = DevRevToDeviceType(device_info[device_count].dev_rev);
// 					device_info[device_count].device_index = device_id;
// 					device_info[device_count].is_no_poll = IsNoPoll(device_info[device_count].dev_rev);
// 					sha204c_set_is_no_poll(device_info[device_count].is_no_poll);
// 
// 					// Read the Selector byte from the configuration zone.
// 					lib_return = sha204m_execute(SHA204_READ, 0, 85 / 4, 0, NULL, 0, NULL, 0, NULL,
// 								sizeof(command), command, sizeof(response), response);
// 					if (lib_return == SHA204_SUCCESS)
// 						device_info[device_count].address = response[2];
// 					else
// 						memset(&device_info[device_count], 0, sizeof(device_info_t));
// 				}
// 			}
// 			else {
// 				// Since a device has woken up with an expected response and it did not reply
// 				// to a DevRev / Info command, we probably found an SA10x device. Let's confirm this
// 				// by sending a GenPersonizationKey command that is the same for all three
// 				// types of SA10x devices, SA100S, SA102S, and SA10HS.
// 				uint8_t count = sa10_genkey_command[SHA204_COUNT_IDX];
// 				uint8_t len = count - SHA204_CRC_SIZE;
// 				sha204c_calculate_crc(len, sa10_genkey_command, sa10_genkey_command + len);
// 				lib_return = sha204p_send_command(count, sa10_genkey_command);
// 				if (lib_return == SHA204_SUCCESS) {
// 					_delay_ms(13);
// 					lib_return = sha204p_receive_response(SHA204_RSP_SIZE_MIN, response);
// 					if (lib_return == SHA204_SUCCESS) {
// 						if (VerifyResponse(response) == SHA204_SUCCESS) {
// 							// We found a SA10xS device.
// 							device_info[device_count].bus_type = DEVKIT_IF_SWI;
// 							device_info[device_count].device_type = DEVICE_TYPE_SA102S;
// 							device_info[device_count].device_index = device_id;
// 						}
// 					}
// 				}
// 			}
// 		}
// 		sha204p_sleep();
// 		sha204p_disable_interface();
// 
// 		if (device_info[device_count].bus_type == DEVKIT_IF_UNKNOWN)
// 			continue;
// 
// 		if (++device_count >= DISCOVER_DEVICE_COUNT_MAX)
// 			break;
// 	}
//  	return device_count;
}


/** \brief This function tries to detect an AES132 I2C device.
 * \param[in] i2c_address I2C address
 */
uint8_t DetectI2cAes132(uint8_t i2c_address)
{
// 	if (device_count >= DISCOVER_DEVICE_COUNT_MAX)
// 		return 0;
// 
// 	uint8_t i2c_status, aes_status;
// 	uint8_t aes_command[3];
//     
//     
//     
// 	aes_command[0] = i2c_address;
// 	aes_command[1] = AES132_STATUS_ADDR >> 8;
// 	aes_command[2] = AES132_STATUS_ADDR & 0xFF;
// 
// 	// Read device status register.
// 	i2c_send_start();
// 	i2c_status = i2c_send_bytes(sizeof(aes_command), aes_command);
// 	if (i2c_status != I2C_FUNCTION_RETCODE_SUCCESS) {
// 		i2c_send_stop();
// 		return i2c_status;
// 	}
// 	i2c_address |= I2C_READ_FLAG;
// 	i2c_send_start();   // repeated start
// 	i2c_status = i2c_send_bytes(1, &i2c_address);   // I2C read address
// 	if (i2c_status != I2C_FUNCTION_RETCODE_SUCCESS) {
// 		i2c_send_stop();
// 		return i2c_status;
// 	}
// 	i2c_status = i2c_receive_bytes(1, &aes_status);
// 	if (i2c_status == I2C_FUNCTION_RETCODE_SUCCESS) {
// 		// Found AES132. Update device_info array.
// 		device_info[device_count].address = i2c_address & ~I2C_READ_FLAG;
// 		device_info[device_count].bus_type = DEVKIT_IF_I2C;
// 		device_info[device_count].device_type = DEVICE_TYPE_AES132;
// 	}
// 	i2c_send_stop();
// 
// 	return i2c_status;
}


/** \brief This function sends a DevRev command to a SHA204 or ECC108 I2C device
 *         and receives its response.
 * \param[in] i2c_address I2C address
 * \return I2C status
 */
uint8_t DetectI2cCryptoAuth(uint8_t i2c_address)
{
// 	if (device_count >= DISCOVER_DEVICE_COUNT_MAX)
// 		return 0;
// 
// 	uint8_t sha204_command[2] = {i2c_address, 3};
// 	uint8_t sha204_response[DEVREV_RSP_SIZE];
// 	uint8_t response_status;
// 	uint8_t wakeup_response_expected[] = {0x04, 0x11, 0x33, 0x43};
// 	uint8_t i2c_status = i2c_send_start();
// 	if (i2c_status != I2C_FUNCTION_RETCODE_SUCCESS)
// 		return i2c_status;
// 
// 	// Receive Wakeup response.
// 	i2c_address |= I2C_READ_FLAG; // I2C read address
// 	i2c_status = i2c_send_bytes(1, &i2c_address);
// 	if (i2c_status != I2C_FUNCTION_RETCODE_SUCCESS) {
// 		i2c_send_stop();
// 		return i2c_status;
// 	}
// 	i2c_status = i2c_receive_bytes(SHA204_RSP_SIZE_MIN, sha204_response);
// 	if (i2c_status != I2C_FUNCTION_RETCODE_SUCCESS)
// 		return i2c_status;
// 	if (memcmp(sha204_response, wakeup_response_expected, SHA204_RSP_SIZE_MIN))
// 	   return I2C_FUNCTION_RETCODE_COMM_FAIL;
// 
// 	// Send DevRev command.
// 	i2c_status = i2c_send_start();
// 	if (i2c_status != I2C_FUNCTION_RETCODE_SUCCESS)
// 		return i2c_status;
// 	i2c_status = i2c_send_bytes(sizeof(sha204_command), sha204_command);
// 	if (i2c_status != I2C_FUNCTION_RETCODE_SUCCESS) {
// 		i2c_send_stop();
// 		return i2c_status;
// 	}
// 	i2c_status = i2c_send_bytes(sizeof(sha204_devrev_command), sha204_devrev_command);
// 	i2c_send_stop();
// 	if (i2c_status != I2C_FUNCTION_RETCODE_SUCCESS) {
// 		/** \todo Should we send a Sleep command here? */
// 		return i2c_status;
// 	}
// 
// 	_delay_ms(DEVREV_EXEC_MAX);
// 
// 	// Receive response.
// 	i2c_send_start();
// 	i2c_status = i2c_send_bytes(1, &i2c_address);
// 	if (i2c_status != I2C_FUNCTION_RETCODE_SUCCESS) {
// 		i2c_send_stop();
// 		return i2c_status;
// 	}
// 	// i2c_receive_bytes sends at Stop after the last byte has been received.
// 	i2c_status = i2c_receive_bytes(sizeof(sha204_response), sha204_response);
// 	if (i2c_status != I2C_FUNCTION_RETCODE_SUCCESS)
// 		return i2c_status;
// 
// 	// Send Sleep command.
// 	sha204_command[1] = 1;
// 	i2c_send_start();
// 	i2c_status = i2c_send_bytes(2, sha204_command);
// 	i2c_send_stop();
// 
// 	// Validate response.
// 	response_status = VerifyResponse(sha204_response);
// 	if (response_status != SHA204_SUCCESS)
// 		return I2C_FUNCTION_RETCODE_COMM_FAIL;
// 
// 	// Found SHA204 or ECC108. Update device_info array.
// 	device_info[device_count].address = i2c_address & ~I2C_READ_FLAG;
// 	device_info[device_count].bus_type = DEVKIT_IF_I2C;
// 	memcpy(device_info[device_count].dev_rev, &sha204_response[SHA204_BUFFER_POS_DATA], sizeof(device_info[device_count].dev_rev));
// 	device_info[device_count].device_type = DevRevToDeviceType(device_info[device_count].dev_rev);
// 	return i2c_status;
}


/** \brief This function loops through all possible I2C addresses
 *         and tries to send a command to a device when an address was acknowledged.
 *  \return number of devices found
 */
uint8_t DetectI2cDevices()
{
	uint8_t data_info[10], data_wake[4];
	uint8_t sleep_flag_command = 0x01;
	uint8_t device_address;
	//	uint32_t bdrt = 400000;	// init i2c baudrate
	uint8_t i2c_count = 0;
		
	// configure i2c interface
	i2c_master_get_config_defaults(&config_i2c_master_discover);
	config_i2c_master_discover.buffer_timeout = 10000;
	config_i2c_master_discover.baud_rate = 100; // 400
	i2c_master_init( &i2c_master_discover, SERCOM5, &config_i2c_master_discover);
	i2c_master_enable(&i2c_master_discover);
	
	// universal packet
	struct i2c_master_packet packet = {
		.address			= 0x00,
		.data_length		= 0,
		.data				= &data_wake[0],
		.ten_bit_address	= false,
		.high_speed			= false,
		.hs_master_code		= 0x0,
	};
	
	// wake device
	packet.address = 0x00; // set wake signal 0x00
	packet.data_length = 0;

	// send wake signal
	i2c_master_write_packet_wait(  &i2c_master_discover, &packet );
	// minimum delay for SHA device wake
	delay_us(2560);

	// valid 7-bit addresses go from 0x07 to 0x78
	for (device_address = 0x07; device_address <= 0x78; device_address++)
	{
		// get wake response
		packet.address = device_address;	// device address in 7-bit format
		packet.data_length = 4;
		packet.data = data_wake;
		i2c_master_read_packet_wait( &i2c_master_discover, &packet );

		// if device succeed to be awaken
		if ( memcmp( data_wake, wake_com_expected, 4 ) == 0 )
		{
			memset(data_wake, 0x00, sizeof(data_wake));	// reset wake data variable

			// send info command
			packet.data_length = 8;
			packet.data = info_command;
			i2c_master_write_packet_wait(  &i2c_master_discover, &packet );
			
			delay_ms(2);
			
			// get info command response
			packet.data_length = 7;
			packet.data = data_info;
			if ( i2c_master_read_packet_wait( &i2c_master_discover, &packet) == STATUS_OK )
			{
				// send sleep flag
				packet.data_length = 1;
				packet.data = &sleep_flag_command;
				i2c_master_write_packet_wait( &i2c_master_discover, &packet );
			}
			
			// detect ECC508A
			// 0x00005000 - 0x000050FF
			if (data_info[3] == 0x50)
			{
				device_info[device_count].bus_type = DEVKIT_IF_I2C;
				/*device_info[device_count].device_type = DEVICE_TYPE_ECC508A;*/
				device_info[device_count].device_type = DEVICE_TYPE_ECC108;
				device_info[device_count].address = packet.address << 1;

				// restore data_info value
				memset(data_info, 0xFF, sizeof(data_info));

				i2c_count++;
				device_count++;
			}
			
			// detect ECC108
			// 0x00001000 - 0x00001001
			else if (data_info[3] == 0x10)
			{
				if ((data_info[4] == 0x00) || (data_info[4] == 0x01))
				{
					device_info[device_count].bus_type = DEVKIT_IF_I2C;
					device_info[device_count].device_type = DEVICE_TYPE_ECC108;
					device_info[device_count].address = packet.address << 1;

					// restore data_info value
					memset(data_info, 0xFF, sizeof(data_info));

					i2c_count++;
					device_count++;
				}
			}

			// detect ECC108A
			// 0x00001002 - 0x000010FF
			else if (data_info[3] == 0x10)
			{
				if ((data_info[4] >= 0x02) && (data_info[4] <= 0xFF))
				{
					device_info[device_count].bus_type = DEVKIT_IF_I2C;
					device_info[device_count].device_type = DEVICE_TYPE_ECC108A;
					device_info[device_count].address = packet.address << 1;

					// restore data_info value
					memset(data_info, 0xFF, sizeof(data_info));

					i2c_count++;
					device_count++;
				}
			}
			
			// detect SHA204A
			// 0x00020008 - 0x0002FFFF
			else if (data_info[2] == 0x02)
			{
				if ((data_info[4] >= 0x08) && (data_info[4] <= 0xFF))
				{
					device_info[device_count].bus_type = DEVKIT_IF_I2C;
					device_info[device_count].device_type = DEVICE_TYPE_SHA204;
					device_info[device_count].address = packet.address << 1;

					// restore data_info value
					memset(data_info, 0xFF, sizeof(data_info));

					i2c_count++;
					device_count++;
				}
			}

			// detect SHA204
			// 0x00020000 - 0x020007
			else if (data_info[3] == 0x00)
			{
				if ((data_info[4] >= 0x00) && (data_info[4] <= 0x07))
				{
					device_info[device_count].bus_type = DEVKIT_IF_I2C;
					device_info[device_count].device_type = DEVICE_TYPE_SHA204;
					device_info[device_count].address = packet.address << 1;

					// restore data_info value
					memset(data_info, 0xFF, sizeof(data_info));
										
					i2c_count++;
					device_count++;
				}
			}
		}
	}
	
	// Check AES132A I2C Interface
	for (device_address = 0x07; device_address <= 0x78; device_address++)
	{
		// send AES132 info command
		packet.address = device_address;
		packet.data_length = 11;
		packet.data = info_command_aes132;
		i2c_master_write_packet_wait( &i2c_master_discover, &packet );
		
		delay_ms(2);
		
		// get AES132 info command
		packet.data_length = 6;
		packet.data = data_info;
		if ( i2c_master_read_packet_wait( &i2c_master_discover, &packet) == STATUS_OK )
		{
			// send sleep flag
			packet.data_length = 1;
			packet.data = &sleep_flag_command;
			i2c_master_write_packet_wait( &i2c_master_discover, &packet );
		}
		
		// detect AES132A
		if (data_info[2]==0x0A)
		{
			device_info[device_count].bus_type = DEVKIT_IF_I2C;
			device_info[device_count].device_type = DEVICE_TYPE_AES132;
			device_info[device_count].address = packet.address << 1;
			
			// restore data_info value
			memset(data_info, 0xFF, sizeof(data_info));

			i2c_count++;
			device_count++;
		}
		
	}

	return i2c_count;
}


/** \brief This function tries to find an AES132 SPI device.
 *  \return number of devices found (maximum 1)
 */
uint8_t DetectSpiDevice()
{
// 	uint8_t status;
// 	uint8_t rx_buffer[AES132_RESPONSE_SIZE_INFO];
// 	memset(rx_buffer, 0, sizeof(rx_buffer));
// 
// 	aes132p_set_interface(DEVKIT_IF_SPI);
// 
// 	status = aes132m_info(AES132_INFO_DEV_NUM, rx_buffer);
// 	if (status > AES132_DEVICE_RETCODE_TEMP_SENSE_ERROR) {
// 		aes132p_disable_interface();
// 		return 0;
// 	}
// 
// 	device_info[device_count].bus_type = DEVKIT_IF_SPI;
// 	device_info[device_count].device_type = DEVICE_TYPE_AES132;
// 	device_info[device_count].device_index = 0;
// 
// 	return ++device_count;
}


/** \brief This function tries to find SHA204, ECC108, and / or AES132 devices.
 *
 *         It calls functions for all three interfaces,
 *         SWI, I2C, and SPI. They in turn enter found
 *         devices into the \ref device_info array.
 * \return interface found
 */
interface_id_t DiscoverDevices()
{
	uint8_t ret_code = 0;
	uint8_t ret_code_i2c;
	uint8_t ret_code_spi;
	uint8_t ret_code_swi;
	uint8_t n = 0; // foor loop

	device_count = 0;
	memset(device_info, 0, sizeof(device_info));

	ret_code_i2c = DetectI2cDevices();

	// test address device detection
// 	printf("device-1: %.2X\n", device_info[0].address);
// 	printf("device-2: %.2X\n", device_info[1].address);
//	printf("device-3: %.2X\n", device_info[2].address);
	return device_info[n].bus_type;
}

interface_id_t SetShaSwiDevice()
{
// 	interface_id_t bus_type;
// 	uint8_t lib_return;
// 	uint8_t i;
// 
// 	// Disable current interface.
// 	if (device_count > 0) {
// 		disable_interface();
// 	}
// 
// 	device_count = 0;
// 	memset(device_info, 0, sizeof(device_info));
// 
// 	DetectSwiDevices();
// 
// 	if (device_count == 0)
// 		return DEVKIT_IF_UNKNOWN;
// 
// 	bus_type = device_info[0].bus_type;
// 
// 	// Switch to interface and test it with a high-level function.
// 	if (device_info[0].device_type == DEVICE_TYPE_SHA204) {
// 		set_and_enable_interface(DEVKIT_LIB_SHA204, bus_type);
// 
// 		for (i = 0; i < device_count; i++) {
// 			sha204p_set_device_id((bus_type == DEVKIT_IF_I2C) ? device_info[i].address : device_info[i].device_index);
// 			sha204c_set_is_no_poll(device_info[i].is_no_poll);
// 			lib_return = sha204c_wakeup(receivebuf);
// 			if (lib_return != SHA204_SUCCESS) {
// 				sha204p_sleep();
// 				sha204p_disable_interface();
// 
// 				// Reset device info.
// 				device_count = 0;
// 				memset(device_info, 0, sizeof(device_info));
// 				return DEVKIT_IF_UNKNOWN;
// 			}
// 			uint8_t command[SHA204_CMD_SIZE_MIN];
// 			// This delay is needed for an ECC108 device.
// 			_delay_us(100);
// 			lib_return = sha204m_execute(SHA204_DEVREV, 0, 0, 0, NULL, 0, NULL, 0, NULL, sizeof(command), command, DEVREV_RSP_SIZE, receivebuf);
// 			sha204p_sleep();
// 			if (lib_return != SHA204_SUCCESS) {
// 				sha204p_disable_interface();
// 				// Reset device info.
// 				device_count = 0;
// 				device_info[0].bus_type = DEVKIT_IF_UNKNOWN;
// 				return DEVKIT_IF_UNKNOWN;
// 			}
// 		}
// 	}
// 	return device_info[0].bus_type;
}


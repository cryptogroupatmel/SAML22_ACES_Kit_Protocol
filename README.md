AT88CK101 porting to SAML22 Xplained Pro
===========================================

Project Description:
This project intend to port uBase-AT88Ck101 project to the SAML22 Xplained Pro. It includes some new features based on SAML22's peripheral component (ex: SLCD).

Prerequisites for this example
--------------------------------

Software:
  - Atmel Studio 7
  
Hardware:
  - Atmel SAML22 Xplained Pro-B
  - Atmel AT88CKSCKTSOIC extension board
  - Atmel AT88CK101 extension board

Software Development Components:
  - CryptoAuthLib Firmware Library 20160108
    Reference Link: http://www.atmel.com/tools/cryptoauthlib.aspx
  - ASF (Atmel Software Framework) 3.32.0

How-to-use
--------------------------

Connect your host computer to the EDBG USB of the SAML22 to program the application using Atmel Studio. 
After compiling the application, point to the Device Programming menu to start programming the .hex file into SAML22 board.

Connect USB host with the SAML22 Xplained Pro EDBG USB and open the serial terminal software on your host
to help see the debug message of the application. 

The communication parameters are:
  - 115,200 baud
  - 8 bit word
  - No parity
  - 1 stop bit

Open the ACES GUI software to start the device discovery process.

Testing Method
-------------------

The methods that is used to test the capability of the software when handling USB stream message from the ACES GUI:
  - configuration of the config zone is based on the config zone value of the "cryptoauth-d21-host" project.
  - the unit_test and the basic_test functions are implemented to check response validities for each of the ECC508 commands.
  - for the ecdh command, it used ECDH application notes from Microchip

Progress Log:
--------------
  1. 20170324: Add device discovery feature for ECC, AES, and SHA devices that operate on SWI, TWI and SPI protocol
  2. 20170515: Add "USB packet process" feature to handle i2c ECC508A commands